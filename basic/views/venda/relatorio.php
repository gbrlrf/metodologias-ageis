<?php

    use yii\helpers\Html;
    use yii\grid\GridView;

    /* @var $this yii\web\View */
    /* @var $searchModel app\models\VendaSearch */
    /* @var $dataProvider yii\data\ActiveDataProvider */

    $atual = date('d/m/y');

    $this->title = 'Relatório de Vendas';

    $this->params['breadcrumbs'][] = $this->title;

?>

<div class="venda-index">

    <h1><?= Html::encode($this->title) ?></h1>

    

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel, //Campo de pesquisa
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn'], //ID dos registros

            'codigo',
            'turno',
            'data',

        //    ['class' => 'yii\grid\ActionColumn'], //Funções de CRUD
        
        ],
    ]); ?>


</div>
