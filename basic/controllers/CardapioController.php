<?php

namespace app\controllers;

use Yii;
use app\models\Cardapio;
use app\models\CardapioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * CardapioController implements the CRUD actions for Cardapio model.
 */
class CardapioController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
               'class' => AccessControl::className(),
               'rules' => [
                   [
                        'allow' => false,   
                        'actions' => ['create', 'update', 'delete'],  
                        'matchCallback' => function ($rule, $action) {
                           if(!Yii::$app->user->isGuest){
                               return Yii::$app->user->identity->tipo !== 'nutricionista';
                           }
                       },
                   ], 
                    [
                       'allow' => true,
                       'roles' => ['@'],
                    ],               
               ],
           ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cardapio models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CardapioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Cardapio model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Cardapio model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Cardapio();


          if ($model->load(Yii::$app->request->post())) {


           
           

            //$model = Venda::model()->find('turno like :turnoP', array(':turnoP'=>'Janta'));

            

            $Cardapio = Cardapio::find()->where(['data' => $model->data ])->one();
           
            if($Cardapio != null){
                  return $this->render('create', [
                            'model' => $model,
                             Yii::$app->session->setFlash('error', 'Cardápio já cadastrado nesta data'),
                        ]);

              

            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
              Yii::$app->session->setFlash('success', 'Cadastro de cardapio realizada com sucesso!');
            return $this->redirect(['view', 'id' => $model->id]);

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Cardapio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Cardapio model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Cardapio model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cardapio the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cardapio::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
