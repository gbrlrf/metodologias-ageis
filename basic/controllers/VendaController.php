<?php

namespace app\controllers;

use Yii;
use app\models\Venda;
use app\models\Lote;
use app\models\VendaSearch;
use app\models\VendaRelatorioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

/**
 * VendaController implements the CRUD actions for Venda model.
 */
class VendaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
               'class' => AccessControl::className(),
               'rules' => [
                   [
                       'allow' => true,
                       'matchCallback' => function ($rule, $action) {
                           if(!Yii::$app->user->isGuest){
                               return Yii::$app->user->identity->tipo === 'admin';
                           }
                       },
                   ],
                   [
                        'allow' => true,
                        'actions' => ['relatorio'],                       
                        'matchCallback' => function ($rule, $action) {
                           if(!Yii::$app->user->isGuest){
                               return Yii::$app->user->identity->tipo === 'nutricionista';
                           }
                       },
                   ],

               ],
           ],
            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Venda models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VendaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Venda models.
     * @return mixed
     */
    public function actionRelatorio()
    {

        $atual = date('y/m/d');

        //$vendas = Venda::find()->where('turno like :turnoP and data =: dataP', array(':turnoP'=>'almoço',':dataP'=>$atual));
        //echo $vendas->createCommand()->sql;

        //die();

        $searchModel = new VendaRelatorioSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('relatorio', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    /**
     * Displays a single Venda model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Venda model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Venda();

        

        if ($model->load(Yii::$app->request->post())) {


            //$minhavenda = new Venda();
            $atual = date('y/m/d');
           

            //$model = Venda::model()->find('turno like :turnoP', array(':turnoP'=>'Janta'));

            

            $lote = Lote::find()->where(['turno' => $model->turno ])->andWhere(['data' => $atual ])->one();
            if($lote == null){
                  return $this->render('create', [
                            'model' => $model,
                             Yii::$app->session->setFlash('error', 'Lote não Existe'),
                        ]);

              

            }else{

                $resultado = $lote->getAttributes();

                if( ($resultado['inicio'] <= $model->codigo) && ($resultado['fim'] >= $model->codigo) ){

                    $venda = Venda::find()->where(['codigo' => $model->codigo ])->andWhere(['turno' => $model->turno ])->one();


                    if($venda == null){

                        $model->data = $atual ;
                        $model->save();
                        Yii::$app->session->setFlash('success', 'Venda realizada com sucesso!');

                        return $this->redirect(['view', 'id' => $model->id]);

                    }else{

                        
                        return $this->render('create', [
                            'model' => $model,
                             Yii::$app->session->setFlash('error', 'TICKET Já Vendido'),
                        ]);


                    }
                }else{
                     return $this->render('create', [
                            'model' => $model,
                             Yii::$app->session->setFlash('error', 'Não está dentro do lote'),
                        ]);

                  

                }

            }

            die();



            
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Venda model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Venda model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Venda model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Venda the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Venda::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
