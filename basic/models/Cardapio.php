<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cardapio".
 *
 * @property int $id
 * @property string $data
 * @property string $descricao
 */
class Cardapio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cardapio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['data', 'descricao'], 'required'],
            [['data'], 'safe'],
            [['data'], 'date', 'format' => 'yyyy/MM/dd', 'message'=> 'Ano-Mês-Dia'],
            [['descricao'], 'string', 'max' => 800],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'data' => 'Data',
            'descricao' => 'Descricao',
        ];
    }
}
