<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lote".
 *
 * @property int $id
 * @property int $inicio
 * @property int $fim
 * @property string $turno
 * @property string $data
 */
class Lote extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lote';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['inicio', 'fim', 'turno', 'data'], 'required'],
            [['inicio', 'fim'], 'integer'],
            [['turno'], 'string'],
            [['data'], 'safe'],
            [['data'], 'date', 'format' => 'yyyy/MM/dd', 'message'=> 'Ano-Mês-Dia'],
            [['data', 'turno'], 'unique', 'targetAttribute' => ['data', 'turno']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'inicio' => 'Inicio',
            'fim' => 'Fim',
            'turno' => 'Turno',
            'data' => 'Data',
        ];
    }
}
